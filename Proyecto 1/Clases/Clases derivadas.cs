﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_1.Clases
{
    public class Cliente : Persona
    {
        #region Propiedades

        private DateTime _F_UltFactura;
        public DateTime F_UltFactura
        {
            get { return _F_UltFactura.Date; }
            //set { _F_UltFactura = value; }
        }

        #endregion

        #region Constructores

        public Cliente(int Id) :base(Id)
        {
            //this.Id = Id;
        }

        public Cliente(int Id, string nombre) : base(Id)
        {
            this.Nombre = nombre;        
        }

        //public Cliente(int Id, string nombre) : this(Id)
        //{
        //    this.Nombre = nombre;
        //}

        //public Cliente(int Id, string nombre) : base(Id, nombre)
        //{
        //    //this.Nombre = nombre;
        //}

        #endregion

        #region Métodos

        //public bool Facturar()
        //{
        //    string mensajeError;
        //    return Facturar(DateTime.Now, out mensajeError);
        //}

        public bool Facturar(out string mensajeError)
        {
            return Facturar(DateTime.Now, out mensajeError);
        }

        public bool Facturar(DateTime fechaFactura, out string mensajeError)
        {
            mensajeError = "";
            if (fechaFactura.Date < this.F_Creacion.Date)
            {
                mensajeError = "Fecha de factura anterior a la de creación.";
                return false;
            }
            if (fechaFactura.Date < this.F_UltFactura)
            {
                mensajeError = "Fecha de factura anterior a la de última factura.";
                return false;
            }

            //Aquí se haría la facturación...
            _F_UltFactura = fechaFactura.Date;
            return true;        
        }



        #endregion  


    }
}