﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;

namespace Proyecto_1.Clases
{
    public class Persona
    {

        #region Constantes

        public const string Tipo = "Persona";

        #endregion

        #region Propiedades

        private int _Id = -1;
        /// <summary>
        /// Obtiene el Id de la persona.
        /// </summary>
        internal int Id
        {
            get { return _Id; }
            private set 
            {
                if (value < 1)
                    throw new ArgumentOutOfRangeException("Id", value, "Sólo se permiten Ids positivos.");
                _Id = value; 
            }
        }

        string _Nombre;
        /// <summary>
        /// Obtiene o establece el Nombre de la persona.
        /// </summary>
        /// <exception cref="System.ArgumentException"></exception>
        public string Nombre
        {
            get { return _Nombre; }
            set 
            {
                //if (value == "")
                //    throw new ArgumentException("Cadena vacía no permitida.", "Propiedad Nombre");

                //if (value.Trim() == "")
                //    throw new ArgumentException("Cadena vacía no permitida.", "Propiedad Nombre");

                //if (value == null || value.Trim() == "")
                //    throw new ArgumentException("Cadena vacía no permitida.", "Propiedad Nombre");

                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentException("Cadena vacía no permitida.", "Propiedad Nombre");
                
                _Nombre = value.Trim(); 
            }
        }

        //private string _Apellidos;
        public string Apellidos {get; set;}
        //{
        //    get { return _Apellidos; }
        //    set { _Apellidos = value; }
        //}

        DateTime _F_Creacion;
        public DateTime F_Creacion
        {
            get { return _F_Creacion; }
            //set { _F_Creacion = value; }
        }

        public string Inicial
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_Nombre))
                    return "";
                return _Nombre.Substring(0, 1).ToUpper() + ".";
            }
        }

        public string NombreCompleto
        {
            get 
            {
                return this.Nombre.Trim() +
                    (!string.IsNullOrWhiteSpace(this.Nombre) || 
                     !string.IsNullOrWhiteSpace(this.Apellidos) 
                     ? " " : "") +
                    this.Apellidos.Trim();            
            }      
        }

        #endregion

        #region Constructores

        //protected Persona()
        //{ }

        /// <summary>
        /// Crea e inicializa una instancia de Persona.
        /// </summary>
        /// <param name="Id">Id a asignar a la persona.</param>
        public Persona(int Id)
        {
          this.Id = Id;
          _F_Creacion = DateTime.Now;
          _Contador++;
        }

        /// <summary>
        /// Crea e inicializa una instancia de Persona.
        /// </summary>
        /// <param name="Id">Id a asignar a la persona.</param>
        /// <param name="nombre">Nombre a asignar a la persona.</param>
        public Persona(int Id, string nombre):this(Id)
        {
            //this.Id = Id;
            //_F_Creacion = DateTime.Now;
            this.Nombre = nombre;
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Método que inicializa los contenidos de todas los valores de la instancia.
        /// </summary>
        public void Limpiar()
        {
            this._Id = -1;
            this._Nombre = null;
            this.Apellidos = null;
            this._F_Creacion = DateTime.MinValue;
        }

        public string Info(bool incluirInicial = false, string separador = "\n")
        {
            //if (!Persona.EstaInicializada(this))
            //    return null;
            if (_Id == -1)
                return null;

            string cad;
            cad = "Id: " + this.Id +
                  separador + "Nombre: " + this.Nombre +
                  (!string.IsNullOrWhiteSpace(this.Apellidos) ?
                       separador + "Apellidos: " + this.Apellidos : "") +
                  (incluirInicial ? separador + "Inicial: " + this.Inicial : "") +
                  separador + "Creación: " + _F_Creacion.ToLongDateString() +
                  " a las " + _F_Creacion.ToLongTimeString();
            return cad;
        }

        #endregion 

        #region Contenido estático

        static int _Contador = 0;

        public static int Contador
        {
            get { return Persona._Contador; }
            //private set { Persona._Contador = value; }
        }

        public static bool EstaInicializada(Persona p)
        {
            //if (p == null)
            //    return false;

            //if (p.Id == -1)
            //    return false;

            //return true;

            if (p == null || p.Id == -1)
                return false;

            return true;

            //return (!(p == null || p.Id == -1));
        }

        #endregion
    }



}