﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Proyecto_1.Clases;

namespace Proyecto_1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = "Hora: " + DateTime.Now.ToLongTimeString();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Label1.Text = "Fecha: " + DateTime.Now.ToLongDateString() +
                          ", Hora: " + DateTime.Now.ToLongTimeString();

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            //int x = 5;
            //long l = x;
            //int y = (int)l;
            //Single s;
            //s = y;
            //x = (int)s;
            //string cad = x.ToString();
            //y = int.Parse(cad);
            //y = Convert.ToInt32(cad);

            //Label2.Text = "<br />Todo Ok";

            string texto = TextBox1.Text;

            int x;
            long l;
            Single s;
            DateTime d;

            //x = int.Parse(texto);
            bool ok = int.TryParse(texto, out x);
            if (ok)
                Label2.Text = "<br />int ok";
            else
                Label2.Text = "<br />int ERROR";

            //l = long.Parse(texto);
            //Label2.Text = Label2.Text + "<br />long ok";
            if (long.TryParse(texto, out l))
                Label2.Text += "<br />long ok";
            else
                Label2.Text += "<br />long ERROR";

            if (Single.TryParse(texto, out s))
                Label2.Text += "<br />Single ok";
            else
                Label2.Text += "<br />Single ERROR";

            //if (DateTime.TryParse(texto, out d))
            //    Label2.Text += "<br />DateTime ok";
            //else
            //    Label2.Text += "<br />DateTime ERROR";

            Label2.Text += "<br />DateTime " + 
                           (DateTime.TryParse(texto, out d) ? "ok" : "ERROR");


            try
            {
                x = int.Parse(texto);
                Label2.Text += "<br />int (con excepción): OK";
            }
            catch (FormatException ex)
            {
                Label2.Text += "<br />int (con excepción): Error de FORMATO. " + ex.Message;
            }
            catch (Exception ex)
            {
                Label2.Text += "<br />int (con excepción): " + ex.Message;
            }         
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            //Rompe si no es un button...
            //Button b = (Button)sender;

            //Con esto NO rompe... simplemente lo deja a null.
            //Button b = sender as Button;
            //if (b != null)
            //{
            //    //...
            //}

            try
            {
                //Proyecto_1.Clases.Persona p1;
                //Clases.Persona p2;
                //Persona p3;

                string cad = "Cantidad: " + Persona.Contador;
                Persona p = null;
                cad += "<br />Inicializada: " + Persona.EstaInicializada(p);
                
                p = new Persona(int.Parse(TextBox2.Text));
                cad += "<br />Inicializada: " + Persona.EstaInicializada(p);
                cad += "<br />" + p.Info(separador: "\t");
                
                p.Nombre = TextBox3.Text;
                p.Apellidos = TextBox4.Text;
                cad += "<br />" + p.Info(separador: "\t");

                p.Limpiar();
                cad += "<br />Inicializada: " + Persona.EstaInicializada(p);
                cad += "<br />" + p.Info(separador: "\t");
                cad += "<br />Cantidad: " + Persona.Contador;

                Label3.Text = cad;

            }
            catch (Exception ex)
            {
                Label3.Text = "Error: " + ex.Message;
            }
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            Cliente c;
            c = new Cliente(int.Parse(TextBox2.Text));
            c.Nombre = TextBox3.Text;
            c.Apellidos = TextBox4.Text;
            Label3.Text = c.Info(separador: "<br />");
            Label3.Text += "<br />Fecha factura: " +
               c.F_UltFactura.ToShortDateString();

            string mensaje;
            if (c.Facturar(out mensaje))
            {
                Label3.Text += "<br />Facturado el " + 
                               c.F_UltFactura.ToShortDateString();
            }
            else
            {
                Label3.Text += "<br />No facturado: " + mensaje;
            }

            DateTime fecha;
            int dias = 0;
            if (!DateTime.TryParse(TextBox5.Text, out fecha) &&
                !int.TryParse(TextBox5.Text, out dias))
            {
                Label3.Text += "<br />No facturado. Fecha incorrecta.";
            }
            else
            {
                //usar constructores...
                if (fecha == DateTime.MinValue)
                    fecha = DateTime.Now;
                fecha = fecha.AddDays(dias);
                if (c.Facturar(fecha, out mensaje))
                {
                    Label3.Text += "<br />Facturado el " +
                                   c.F_UltFactura.ToShortDateString();
                }
                else
                {
                    Label3.Text += "<br />No facturado: " + mensaje;
                }
            }


            
        }
    }
}